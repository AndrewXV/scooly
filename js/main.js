var Slider = new Object();

Slider.Next = function() {
        $('.slide').each(function(){
            if ($(this).css('display')=='block'){
                Slider.number = parseInt($(this).attr('id'));
            }
        });
        var prev = "#"+Slider.number;
        var next = "#"+(Slider.number+1);
        $(prev).hide();
        $(next).show();
};

Slider.Prev = function() {
        $('.slide').each(function(){
            if ($(this).css('display')=='block'){
                Slider.number = parseInt($(this).attr('id'));
            }
        });
        var next = "#"+Slider.number;
        var prev = "#"+(Slider.number-1);
        $(next).hide();
        $(prev).show();
};

$(document).ready(function(){
    $('#next').click(function(){
        Slider.Next();
    });

    $('#prev').click(function(){
        Slider.Prev();
    });
});

